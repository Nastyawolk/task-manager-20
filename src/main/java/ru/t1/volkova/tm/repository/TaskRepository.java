package ru.t1.volkova.tm.repository;

import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        final List<Task> tasks = findAll(userId);
        for (final Task task : tasks) {
            if (task.getProjectId() == null) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
