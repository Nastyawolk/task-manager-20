package ru.t1.volkova.tm.component;

import ru.t1.volkova.tm.api.repository.ICommandRepository;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.command.project.*;
import ru.t1.volkova.tm.command.system.*;
import ru.t1.volkova.tm.command.task.*;
import ru.t1.volkova.tm.command.user.*;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.volkova.tm.exception.system.CommandNotSupportedException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.CommandRepository;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;
import ru.t1.volkova.tm.repository.UserRepository;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectClearCommand());

        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskClearCommand());

        registry(new UserLoginCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLogoutCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    public void initDemoData() {
        final User userTest = userService.create("test", "test");
        final User userCustom = userService.create("user", "user", "user@user.ru");
        final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "PROJECT12", "Project for TestUser");
        projectService.create(userTest.getId(), "PROJECT12345", "Project 2 for TestUser");
        projectService.create(userCustom.getId(), "PROJECT444", "Project for CustomUser");
        projectService.create(userAdmin.getId(), "PROJECT123", "Project for Admin");
        projectService.create(userAdmin.getId(), "PROJECT1", "Project 2 for Admin");

        taskService.create(userTest.getId(), "TASK123", "test task");
        taskService.create(userTest.getId(), "TASK1234", "test task2");
        taskService.create(userCustom.getId(), "TASK", "test task");
        taskService.create(userCustom.getId(), "TASK5", "test task");
        taskService.create(userAdmin.getId(), "TASK12", "test task");
        taskService.create(userAdmin.getId(), "TASK1", "test task");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(final String[] args) {
        initLogger();
        processArguments(args);
        initDemoData();
        while (!(Thread.currentThread().isInterrupted())) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        processArgument(args[0]);
        exit();
    }

    private void processArgument(final String arg) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void exit() {
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

}
