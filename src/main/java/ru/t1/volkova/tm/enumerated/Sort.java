package ru.t1.volkova.tm.enumerated;

import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANSE),
    BY_NAME("Sort by name", NameComparator.INSTANSE),
    BY_STATUS("Sort by status", StatusComparator.INSTANSE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Sort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
