package ru.t1.volkova.tm.api.repository;

import ru.t1.volkova.tm.enumerated.Sort;
import ru.t1.volkova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    void add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    List<M> findAll(Sort sort);

    void clear();

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M remove(M model);

    M removeById(String id);

    void removeAll(List<M> models);

    M removeByIndex(Integer index);

    Integer getSize();

}
