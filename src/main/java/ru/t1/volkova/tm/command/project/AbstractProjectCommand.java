package ru.t1.volkova.tm.command.project;

import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService projectService() {
        return getServiceLocator().getProjectService();
    }

    protected IProjectTaskService projectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}
