package ru.t1.volkova.tm.command.system;

import ru.t1.volkova.tm.api.service.ICommandService;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
