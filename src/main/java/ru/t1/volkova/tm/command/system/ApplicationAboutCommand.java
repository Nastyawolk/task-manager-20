package ru.t1.volkova.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = "-a";

    private static final String DESCRIPTION = "Show developer info.";

    private static final String NAME = "about";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasia Volkova");
        System.out.println("e-mail: aavolkova@t1-consulting.ru");
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
