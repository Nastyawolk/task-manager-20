package ru.t1.volkova.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Remove all projects.";

    private static final String NAME = "project-clear";

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        final String userId = getUserId();
        projectService().clear(userId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
