package ru.t1.volkova.tm.command.project;

import ru.t1.volkova.tm.enumerated.Sort;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Show list projects.";

    private static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = projectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            String print = index++ + ". " +
                    project.getName() + " | " +
                    project.getId() + " | " +
                    project.getStatus();
            System.out.println(print);
        }
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
