package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Start task status by id.";

    private static final String NAME = "task-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        taskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
