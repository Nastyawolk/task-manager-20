package ru.t1.volkova.tm.command.user;

import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    private static final String DESCRIPTION = "View profile of current user";

    private static final String NAME = "view-user-profile";

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
